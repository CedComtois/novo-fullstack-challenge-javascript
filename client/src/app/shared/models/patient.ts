import { Deserializable } from "./deserializable";

export class Patient implements Deserializable {
  email: String;
  first_name: String;
  last_name: String;
  sex: String;
  age: Number;
  car_freq: Number;
  temperature: Number;
  spo2: Number;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }

  getLatestVitalSigns = function() {
    return this.vital_signs ? this.vital_signs[this.vital_signs.length - 1] : {};
  };

  getMaxValue = function(field: string) {
    if (this.vital_signs)
      return Math.max(...this.vital_signs.map(vs => vs[field]));
  };

  getMinValue = function(field: string) {
    if (this.vital_signs)
      return Math.min(...this.vital_signs.map(vs => vs[field]));
  };

  getAverage = function(field: string) {
    if (!this.vital_signs)
      return;

    var nums = this.vital_signs.map(vs => vs[field]);
    return Math.round(nums.reduce((a, b) => (a + b)) / nums.length);
  };
}