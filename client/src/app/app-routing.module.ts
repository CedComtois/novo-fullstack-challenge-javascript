import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { ProfileComponent } from "./profile/profile.component";
import { PatientComponent } from "./patient/patient.component";
import { PatientAddComponent } from "./patient/patient-add.component";
import { PatientDetailsComponent } from "./patient/patient-details.component";
import { AuthGuard } from "./auth.guard";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "profile", component: ProfileComponent, canActivate: [AuthGuard] },
  { path: "patients", component: PatientComponent, canActivate: [AuthGuard] },
  { path: "patients/add", component: PatientAddComponent, canActivate: [AuthGuard] },
  { path: "patients/:id", component: PatientDetailsComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
