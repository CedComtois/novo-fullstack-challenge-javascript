import { Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router"

@Component({
  templateUrl: "./patient-add.component.html",
  styleUrls: ["./patient-add.component.css"]
})
export class PatientAddComponent {
  patient: { first_name: null, last_name: null, email: null, sex: null, age: null }

  constructor(
    private httpClient: HttpClient,
    private router: Router
  ) { this.patient = { first_name: null, last_name: null, email: null, sex: null, age: null }; }

  savePatient() {
    this.httpClient.post("/api/patients", this.patient).subscribe(
      () => {
        this.router.navigate(['/patients']);
      }
    );
  }
}