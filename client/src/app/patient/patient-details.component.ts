import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { SocketioService } from "../socketio.service";
import { ActivatedRoute } from '@angular/router';
import { Patient } from "../shared/models/patient";

@Component({
  templateUrl: "./patient-details.component.html",
  styleUrls: ["./patient-details.component.css"]
})
export class PatientDetailsComponent implements OnInit {
  patientId: String;
  patient: Patient;
  message: String = null;

  constructor(
    private httpClient: HttpClient,
    private socketService: SocketioService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    var vm = this;

    vm.patient = new Patient();
    vm.route.params.subscribe(params => {
      vm.patientId = params["id"];
      vm.getPatient();
    });

    vm.socketService.socket.on("patient change", function(patientId) {
      if (patientId == vm.patientId) { vm.getPatient(); }
    });
  }

  getPatient() {
    this.httpClient.get(`api/patients/${this.patientId}`).subscribe(patient => {
      this.patient = this.patient.deserialize(patient);
    });
  };

  sendEmail() {
    this.httpClient.post(`api/patients/${this.patientId}/email`, { message: this.message }).subscribe(() => {
      this.message = null;
    });
  };
}