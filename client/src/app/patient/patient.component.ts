import { Component, OnInit, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { SocketioService } from "../socketio.service";
import { Patient } from '../shared/models/patient';

@Component({
  templateUrl: "./patient.component.html",
  styleUrls: ["./patient.component.css"]
})
export class PatientComponent implements OnInit {
  patients: Patient[]
  displayedColumns: string[] = [
    "firstName",
    "lastName",
    "sex",
    "age",
    "email",
    "carFreq",
    "temperature",
    "spo2",
    "delete"
  ];

  constructor(
    private httpClient: HttpClient,
    private socketService: SocketioService
  ) {}

  ngOnInit() {
    var vm = this;

    vm.getPatients();

    vm.socketService.socket.on("patient change", function() {
      vm.getPatients();
    });
  }

  deletePatient(patientId) {
    this.httpClient.delete(`api/patients/${patientId}`).subscribe();
  }

  getPatients() {
    this.httpClient.get<Patient[]>("/api/patients").subscribe(
      patients => {
        this.patients = patients.map(patient => {
          var newPatient = new Patient();
          newPatient = newPatient.deserialize(patient);
          return newPatient;
        });
      }
    );
  }
}