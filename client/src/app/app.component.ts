import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "./authentication.service";
import { SocketioService } from './socketio.service';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})

export class AppComponent implements OnInit {
  title = 'socketio-angular';

  constructor(public auth: AuthenticationService, private socketService: SocketioService) {}

  ngOnInit() {
    this.socketService.setupSocketConnection();
  }
}
