const mongoose = require('mongoose');
var global = require('../../global');

const vitalSignSchema = new mongoose.Schema({
  cardiac_frequency: {
    type: Number,
    required: true
  },
  temperature: {
    type: Number,
    required: true
  },
  spo2: {
    type: Number,
    required: true
  }
});

const patientSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true
  },
  first_name: {
    type: String,
    required: true
  },
  last_name: {
    type: String,
    required: true
  },
  sex: {
    type: String,
    required: true
  },
  age: {
    type: Number,
    required: true
  },
  vital_signs: [vitalSignSchema]
});

const Patient = mongoose.model('Patient', patientSchema);

Patient.watch().on('change', data => {
  global.io.emit('patient change', data.documentKey._id);
});
