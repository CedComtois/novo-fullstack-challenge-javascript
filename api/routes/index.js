const ctrlAuth = require('../controllers/authentication');
const ctrlProfile = require('../controllers/profile');
const ctrlPatient = require('../controllers/patients');
const express = require('express');
const jwt = require('express-jwt');
const router = express.Router();

const auth = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload'
});


// profile
router.get('/profile', auth, ctrlProfile.profileRead);

// authentication
router.post('/register', ctrlAuth.register);
router.post('/login', ctrlAuth.login);

// patients
router.get('/patients', ctrlPatient.listPatients);
router.get('/patients/:id', ctrlPatient.getPatient);
router.post('/patients', ctrlPatient.createPatient);
router.delete('/patients/:id', ctrlPatient.deletePatient);
router.post('/patients/:id/email', ctrlPatient.emailPatient);

module.exports = router;
