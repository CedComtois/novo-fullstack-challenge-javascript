const mongoose = require('mongoose');
const Patient = mongoose.model('Patient');
const cron = require("node-cron");
var global = require('../../global');

module.exports.getPatient = (req, res) => {
  Patient.findById(req.params.id)
         .then(patient => {
           res.status(200).json(patient);
         });
};

module.exports.listPatients = (req, res) => {
  Patient.find().exec(function(err, patients) {
    res.status(200).json(patients);
  });
};

module.exports.createPatient = (req, res) => {
  Patient.create(req.body)
         .then(function(patient) {
           res.status(201).json(patient);
         });
};

module.exports.deletePatient = (req, res) => {
  Patient.deleteOne({ _id: req.params.id }).then(
    () => {
      res.status(204).json({
        message: 'Deleted!'
      });
    }
  );
};

module.exports.emailPatient = (req, res) => {
  Patient.findById(req.params.id)
         .then(patient => {
           var mailOptions = {
             to: patient.email,
             subject: "Message from Online Doctor",
             text: req.body.message
           };
          
           global.mailer.sendMail(mailOptions, function(error, info){
             res.status(200).json(info);
           });
          });
};

cron.schedule("*/5 * * * * *", function() {
  Patient.find().exec(function(err, patients) {
    var ids = patients.map(patient => patient._id);
    Patient.updateOne(
      { _id: ids[Math.floor(Math.random() * ids.length)] },
      { $addToSet: { vital_signs:  {
        cardiac_frequency: randomIntFromInterval(60, 90),
        temperature: randomIntFromInterval(36, 42) ,
        spo2: randomIntFromInterval(69, 91)
      } } }
    ).exec();
  });
});

function randomIntFromInterval(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}