# Challenge Concepteur Full Stack
Ceci est un challenge qui sert d'entrevue technique pour le poste de concepteur logiciel Full Stack chez NOVO. N'hésitez à forker ce repo. Les Pull Request seront ignorées.

## Brief
Concevoir une application web qui permet à un médecin d'effectuer de la télémédecine pour un groupe de patients.

*Les solutions partielles sont acceptées* Ce n'est pas nécessaire de soumettre une solution qui implémente tous les requis.

### Vue Tableau de bord
* Ajouter/Supprimer des patients (Nom, Prénom, Age, Sexe, Courriel)
* Afficher l'ensemble des patients et pour chaque patient, afficher ses signes vitaux (Fréquence cardiaque, Température, SpO2)

### Vue Patient 
* Afficher des statistiques (Min. Max. Avg.) pour chaque signe vital
* Envoyer un message au patient

## Portée du challenge
* Description haut-niveau du design et le design pattern utilisé
* API Serveur(en utilisant le langage de programmation Node.js avec le framework de votre choix tel que Express.js, Hapi.JS, etc...)
	* Implémenter au moins 3 appels d'API dont minimalement la mise à jour périodique des signes vitaux en utilisant des données générées aléatoirement.
	* La plupart des concepteurs chez NOVO utilisent MongoDB ou Firestore comme engin de BD
* Application web (client)
	* Implémenter une Single Page Application en utilisant un framework moderne (React, Angular ou Vue.js) qui communique avec le serveur.
	* Cela devrait s'intégrer avec votre API, mais c'est acceptable de générer du contenu statique pour certaines pages.
* Documenter les hypothèses que tu auras faites
* Une solution peut ne pas être complète, par contre, ce qui est soumis doit fonctionner

*Bref, impressionne-nous!*

## Comment compléter ce challenge
* Forker ce repo sur Bitbucket.
* Complète le design et l'implémentation tel que défini et ce, au meilleur de tes compétences.
* Lorsqu'approprié, ajoute des notes dans ton code pour nous aider à comprendre. Assures-toi que ce soit compréhensible pour l'équipe NOVO.
* Complètes le travail dans ton repo Git personnel et envoies-nous le résultat.
* Tente d'accomplir le maximum en 8h de travail en tablant d'abord sur tes forces. 
	* Tu peux faire plus si tu en a envie ou en faire moins si tu manques de temps. 
	* On va te demander le nombre d'heures fait... ;)
* On va planifier une rencontre d'une heure ensemble pour que tu nous présente le tout
	* L'objectif est de comprendre les décisions que tu as pris et leurs motivations

## Qu'est-ce que nous recherchons? Qu'est-ce que cela prouve? 
* Déterminer ta capacité à faire des hypothèses à partir de requis très limités.
* Déterminer ta capacité à faire des choix technologiques et de design.
* Identifier tes forces.
* Il n'y pas de réussite ou d'échec, cela servira de base de discussion avec toi pour traiter des enjeux spécifiques.